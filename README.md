# **QuickCheckout Extension for Magento 1.9.x** #

## **Description** ##

Checkout has never been so fast and simplified. Quick Checkout allows you to wrap up all your checkout activities on one single page, thus reducing the time and enhancing your shopping experience.