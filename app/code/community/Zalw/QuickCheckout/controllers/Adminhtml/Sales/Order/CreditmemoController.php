<?php
/**
 * Adminhtml sales order creditmemo controller
 *
 * @category   Zalw
 * @package    Zalw_Quickcheckout
 */
 
include_once("Mage/Adminhtml/controllers/Sales/Order/CreditmemoController.php");
class Zalw_QuickCheckout_Adminhtml_Sales_Order_CreditmemoController extends Mage_Adminhtml_Sales_Order_CreditmemoController
{
    /**
     * Save creditmemo
     * We can save only new creditmemo. Existing creditmemos are not editable
     */
    public function saveAction()
    {
        $data = $this->getRequest()->getPost('creditmemo');
        if (!empty($data['comment_text'])) {
            Mage::getSingleton('adminhtml/session')->setCommentText($data['comment_text']);
        }
        try {
            $creditmemo = $this->_initCreditmemo();
            if ($creditmemo) 
            {
                if (($creditmemo->getGrandTotal() <=0) && (!$creditmemo->getAllowZeroGrandTotal())) {
                    Mage::throwException(
                        $this->__('Credit memo\'s total must be positive.')
                    );
                }
                $giftWrap  = $data['gift_wrap'];
                $giftwrapRefundeded = $creditmemo->getOrder()->getGiftwrapAmountRefunded();
                $gift_wrap_amount =$creditmemo->getOrder()->getGiftwrapAmount();

                $giftwrapLeft= $gift_wrap_amount - $giftwrapRefundeded; 

                Mage::log('gift_wrap'.$data['gift_wrap']."------".$giftwrapRefundeded."------".$gift_wrap_amount."------".$giftwrapLeft."------",null,'gift_wrap.log');

                if($giftwrapLeft<$giftWrap ){
                    Mage::throwException(
                        Mage::helper('sales')->__('Maximum Gift Wrap amount allowed to refund is: %s', $giftwrapLeft)
                    );
                }
                $this->_redirect('*/sales_order/view', array('order_id' => $creditmemo->getOrderId()));
                $comment = '';
                if (!empty($data['comment_text'])) {
                    $creditmemo->addComment(
                        $data['comment_text'],
                        isset($data['comment_customer_notify']),
                        isset($data['is_visible_on_front'])
                    );
                    if (isset($data['comment_customer_notify'])) {
                        $comment = $data['comment_text'];
                    }
                }

                if (isset($data['do_refund'])) {
                    $creditmemo->setRefundRequested(true);
                }
                if (isset($data['do_offline'])) {
                    $creditmemo->setOfflineRequested((bool)(int)$data['do_offline']);
                }

                $grandTotal = $creditmemo->getGrandTotal();

                if($giftWrap){
                    Mage::log('gift_wrap'.$grandTotal-$giftwrapLeft+$giftWrap,null,'gift_wrap.log');
                    $creditmemo->setGrandTotal($grandTotal-$giftwrapLeft+$giftWrap);
                    $creditmemo->setBaseGrandTotal($grandTotal-$giftwrapLeft+$giftWrap);
                    $creditmemo->setGiftwrapAmount($giftWrap);
                }
                else{
                    $creditmemo->setGrandTotal($grandTotal-$giftwrapLeft);
                    $creditmemo->setBaseGrandTotal($grandTotal-$giftwrapLeft);
                    $creditmemo->setGiftwrapAmount(0);
                }

                $creditmemo->register();
                if (!empty($data['send_email'])) {
                    $creditmemo->setEmailSent(true);
                }

                $creditmemo->getOrder()->setGiftwrapAmountRefunded($giftwrapRefundeded+$giftWrap);

                $creditmemo->getOrder()->setCustomerNoteNotify(!empty($data['send_email']));
                $this->_saveCreditmemo($creditmemo);
                $creditmemo->sendEmail(!empty($data['send_email']), $comment);
                $this->_getSession()->addSuccess($this->__('The credit memo has been created.'));
                Mage::getSingleton('adminhtml/session')->getCommentText(true);
                $creditmemo->save();
                $this->_redirect('*/sales_order/view', array('order_id' => $creditmemo->getOrderId()));
                return;
            } 
            else 
            {
                $this->_forward('noRoute');
                return;
            }
        } catch (Mage_Core_Exception $e) {
            $this->_getSession()->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setFormData($data);
        } catch (Exception $e) {
            Mage::logException($e);
            $this->_getSession()->addError($this->__('Cannot save the credit memo.'));
        }
        $this->_redirect('*/*/new', array('_current' => true));
    }
}