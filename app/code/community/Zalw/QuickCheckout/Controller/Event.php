<?php
/*
 * @category   ZALW
 * @package    Zalw_Quickcheckout
 * @module     Quickcheckout
 * @author     ALW
 * @description: This is controller class for Quickcheckout module for event predispatch.
 */
class Zalw_QuickCheckout_Controller_Event
{
    /*Event: adminhtml_controller_action_predispatch_start*/
    public function customTheme()
    {
        Mage::getDesign()->setArea('adminhtml')
            ->setTheme((string)Mage::getStoreConfig('quickcheckout/css_style/admin'));
    }
}
