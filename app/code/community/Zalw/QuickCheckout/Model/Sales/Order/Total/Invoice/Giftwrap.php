<?php
/*
 * @category   ZALW
 * @package    Zalw_QuickCheckout
 * @module     QuickCheckout
 * @author     ALW
 * @description: This is Model class for QuickCheckout module for Invoice Total.
 */

class Zalw_QuickCheckout_Model_Sales_Order_Total_Invoice_Giftwrap extends Mage_Sales_Model_Order_Invoice_Total_Abstract
{
    /*Function to add gift wrap price in invoice total when gift wrap included in order*/
    public function collect(Mage_Sales_Model_Order_Invoice $invoice)
    {
        if(Mage::helper('quickcheckout')->getQuickCheckoutConfig('gift_wrap/gift_wrap_option')): 
            $order = $invoice->getOrder();
            $orderId  = $invoice->getOrderId();

            $collection = Mage::getModel('sales/order')->load($orderId);

            $collections = Mage::getModel('sales/order_invoice')
                ->getCollection()
                ->addAttributeToFilter('order_id', $orderId)
                ->getFirstItem();
            if(!$collections->getId()){
                foreach($collection as $coll)
                {
                    $giftWrapOption = $coll['gift_wrap_option'];
                    $giftwrapAmount =  $coll['giftwrap_amount'];
                    break;
                }
                if($giftWrapOption==1){
                    $giftwrapPrice = Mage::getStoreConfig('quickcheckout/gift_wrap/gift_wrap_price');
                    $invoice->setGrandTotal($invoice->getGrandTotal() + $giftwrapAmount);
                    $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal() + $giftwrapAmount);

                    $invoice->setGiftwrapAmount($giftwrapAmount);
                    $invoice->setBaseGiftwrapAmount($giftwrapAmount);
                    $invoice->setGiftWrapOption(1);
                }
                else{
                    $invoice->setGrandTotal($invoice->getGrandTotal());
                    $invoice->setBaseGrandTotal($invoice->getBaseGrandTotal());
                }
            }
            return $this;
        endif;
    }
}
