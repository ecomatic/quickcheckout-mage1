<?php
/*
 * @category   ZALW
 * @package    Zalw_QuickCheckout
 * @module     QuickCheckout
 * @author     ALW
 * @description: This is Model class for QuickCheckout module for Creditmemo Total.
 */
class Zalw_QuickCheckout_Model_Sales_Order_Total_Creditmemo_Giftwrap extends Mage_Sales_Model_Order_Creditmemo_Total_Abstract
{
    /*Function to add gift wrap price in creditmemo total when gift wrap included in order*/
    public function collect(Mage_Sales_Model_Order_Creditmemo $creditmemo)
    {
        if(Mage::helper('quickcheckout')->getQuickCheckoutConfig('gift_wrap/gift_wrap_option')): 
            $order = $creditmemo->getOrder();
            $orderId  = $creditmemo->getOrderId();

            $collection = Mage::getModel('sales/order')->load($orderId);
            $collections = Mage::getModel('sales/order_creditmemo')
                ->getCollection()
                ->addAttributeToFilter('order_id', $orderId)
                ->getFirstItem();
             $collections->getId();

            foreach($collection as $coll)
            {
                $giftWrapOption = $coll['gift_wrap_option'];
                $giftwrapAmountRefunded  =  $coll['giftwrap_amount_refunded'];
                $giftwrapAmount =  $coll['giftwrap_amount'];
                break;
            }

            $giftwrapLeft= $giftwrapAmount - $giftwrapAmountRefunded; 
            if($giftWrapOption==1)
            {
                $giftwrapPrice = Mage::getStoreConfig('quickcheckout/gift_wrap/gift_wrap_price');
                $creditmemo->setGrandTotal($creditmemo->getGrandTotal() + $giftwrapLeft);
                $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal() + $giftwrapLeft);

                $creditmemo->setGiftwrapAmount($giftwrapAmount);
                $creditmemo->setBaseGiftwrapAmount($giftwrapAmount);
                $creditmemo->setGiftWrapOption(1);
            }
            else{
                $creditmemo->setGrandTotal($creditmemo->getGrandTotal());
                $creditmemo->setBaseGrandTotal($creditmemo->getBaseGrandTotal());
            }
            return $this;
        endif;
    }
}