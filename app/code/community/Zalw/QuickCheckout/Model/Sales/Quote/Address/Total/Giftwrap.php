<?php
/*
 * @category   ZALW
 * @package    Zalw_QuickCheckout
 * @module     QuickCheckout
 * @author     ALW
 * @description: This is Model class for QuickCheckout module for Quote Total.
 */

class Zalw_QuickCheckout_Model_Sales_Quote_Address_Total_Giftwrap extends Mage_Sales_Model_Quote_Address_Total_Abstract{

    /* Change the total price according to the giftwrap*/
    public function collect(Mage_Sales_Model_Quote_Address $address)
    {
        parent::collect($address);

        if(Mage::helper('quickcheckout')->getQuickCheckoutConfig('gift_wrap/gift_wrap_option')): 
            $this->_setAmount(0);
            $this->_setBaseAmount(0);

            $items = $this->_getAddressItems($address);
            if (!count($items)) {
                return $this; //this makes only address type shipping to come through
            }

            $quote = $address->getQuote();

            if(Zalw_QuickCheckout_Model_Giftwrap::canApply($address)){
                $giftwrapPrice = Mage::getStoreConfig('quickcheckout/gift_wrap/gift_wrap_price');
                $balance = $giftwrapPrice;

                $address->setGiftwrapAmount($giftwrapPrice);
                $address->setBaseGiftwrapAmount($giftwrapPrice);

                $quote->setGiftwrapAmount($giftwrapPrice);

                $salesFlatQuote = Mage::getSingleton('core/resource')->getTableName('sales_flat_quote');
                $connection    = Mage::getSingleton('core/resource')->getConnection('core_write'); 
                //Database connection
                $giftWrapOption = null;
                if($address->getQuoteId())
                {
                    $sql = "select gift_wrap_option from " . $salesFlatQuote . "  where entity_id=".$address->getQuoteId();

                    $var = $connection->query($sql);
                    foreach($var as $v)
                    {
                        $giftWrapOption = $v['gift_wrap_option'];
                    }
                }
                $myValue=Mage::getSingleton('core/session')->getGiftWrap();

                //if gift wrap is checked then add giftwrap price to the total 
                if($giftWrapOption==1){
                    Mage::getSingleton('core/session')->setGiftWrap('GiftWrap');
                    $giftwrapPrice = Mage::getStoreConfig('quickcheckout/gift_wrap/gift_wrap_price');
                    $address->setGrandTotal($address->getGrandTotal() + $address->getGiftwrapAmount());
                    $address->setBaseGrandTotal($address->getBaseGrandTotal() + $address->getBaseGiftwrapAmount());
                }
                //if gift wrap is unchecked then check if it was checked before 
                else{
                    $myValue=Mage::getSingleton('core/session')->getGiftWrap();
                    //if gift wrap is checked before unset session and set grand total to zero
                    if($myValue!=''){
                        $address->setGrandTotal($address->getGrandTotal());
                        $address->setBaseGrandTotal($address->getBaseGrandTotal());
                        
                        Mage::getSingleton('core/session')->unsGiftWrap();

                    }
                    else{
                        $address->setGrandTotal($address->getGrandTotal());
                        $address->setBaseGrandTotal($address->getBaseGrandTotal());
                    }
                }
            }
        endif;
    }

    /*public function fetch(Mage_Sales_Model_Quote_Address $address) { 
        $amt = $address->getGiftwrapAmount(); 
        $address->addTotal(array( 
            'code'=>$this->getCode(), 
            'title'=>Mage::helper('quickcheckout')->__('Fee'), 
            'value'=> $amt 
            )
        ); 
        return $this; 
    }*/
}