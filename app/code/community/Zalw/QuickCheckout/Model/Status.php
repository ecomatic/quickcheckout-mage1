<?php
/*
 * @category   ZALW
 * @package    Zalw_Quickcheckout
 * @module     Quickcheckout
 * @author     ALW
 * @description: This is Model class for Quickcheckout module for status check.
 */
class Zalw_QuickCheckout_Model_Status extends Varien_Object
{
    const STATUS_ENABLED    = 1;
    const STATUS_DISABLED   = 2;
    /*Get option array of status*/
    static public function getOptionArray()
    {
        return array(
            self::STATUS_ENABLED    => Mage::helper('quickcheckout')->__('Enabled'),
            self::STATUS_DISABLED   => Mage::helper('quickcheckout')->__('Disabled')
        );
    }
}
