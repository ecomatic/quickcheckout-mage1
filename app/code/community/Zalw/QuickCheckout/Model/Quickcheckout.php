<?php
/*
 * @category   ZALW
 * @package    Zalw_QuickCheckout
 * @module     QuickCheckout
 */
 
class Zalw_QuickCheckout_Model_Quickcheckout extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('quickcheckout/quickcheckout');
    }

    /*Update gift wrap in the sales_flat_quote table according to the selected or unselected gift wrap checkbox*/
    public function updateGiftWrap($gift_wrap,$quoteId)
    {
        $salesFlatQuote = Mage::getSingleton('core/resource')->getTableName('sales_flat_quote');
        $connection    = Mage::getSingleton('core/resource')->getConnection('core_write'); //Database connection

        $sql = "UPDATE " . $salesFlatQuote . " set gift_wrap_option = ".$gift_wrap." where entity_id=".$quoteId;

        $connection->query($sql);
    }

    /*Get Gift wrap option*/
    public function getGiftwrapOption($quoteId)
    {
        $salesFlatQuote = Mage::getSingleton('core/resource')->getTableName('sales_flat_quote');
        $connection    = Mage::getSingleton('core/resource')->getConnection('core_write'); //Database connection
        $sql = "select gift_wrap_option from " . $salesFlatQuote . "  where entity_id=".$quoteId;

        $var = $connection->query($sql);
        foreach($var as $v)
        {
            $giftWrapOption = $v['gift_wrap_option'];
            break;
        }
        return $giftWrapOption;
    }

    /*Get Product type to hide gift wrap checkbox for virtual and downloadable product in cart*/
    public function getProductType()
    {
        $productIds = Mage::getModel('checkout/cart')->getProductIds(); //get cart product ids
        foreach($productIds as $product_id)
        {
           $product=Mage::getModel('catalog/product')->load($product_id);
           $productType[] = $product->getTypeID();
        }
        $needle  = array('simple','bundle','configurable','grouped');
        $allowGiftwrap = (int)array_intersect((array)$needle, $productType);
          
        return $allowGiftwrap;
    }
}