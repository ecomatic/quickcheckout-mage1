<?php
/*
 * @category   ZALW
 * @package    Zalw_QuickCheckout
 * @module     QuickCheckout
 * @author     ALW
 * @description: This is Model class for QuickCheckout module for observer.
 */

class Zalw_QuickCheckout_Model_Observer
{
    public function QuickCheckoutRedirect($observer) {
        if (Mage::helper('quickcheckout')->getQuickCheckoutConfig('general/active')) {
            Mage::app()->getFrontController()->getResponse()->setRedirect(Mage::getUrl("checkout/onestep"))->sendResponse();
        }
    }
    
    /*Function to update gift option value in the order as per the gift option in the quote table*/
    public function AddGiftwrapInOrderTotal($observer)
    {
        if(Mage::helper('quickcheckout')->getQuickCheckoutConfig('gift_wrap/gift_wrap_option')){
            $order = $observer->getOrder();
            $quoteId = $order->getQuoteId();
                
            $gift_wrap = Mage::getModel('quickcheckout/quickcheckout')->getGiftwrapOption($quoteId);
            $giftwrapPrice = Mage::getStoreConfig('quickcheckout/gift_wrap/gift_wrap_price');
            
            $sales_flat_order = Mage::getSingleton('core/resource')->getTableName('sales_flat_order');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_write'); //Database connection

            $sql = "UPDATE " . $sales_flat_order . " set gift_wrap_option = ".$gift_wrap.", `giftwrap_amount` = ".$giftwrapPrice.", `basegiftwrap_amount` = ".$giftwrapPrice." where entity_id=".$order->getId();

            $connection->query($sql);

            Mage::getSingleton('core/session')->unsGiftWrapSession();
        }
    }

    public function updatePaypalTotal(Varien_Event_Observer $observer){
        $cart = $observer->getEvent()->getPaypalCart();
        $giftWrapAmount = $cart->getSalesEntity()->getGiftWrapAmount();
        /* @var $cart Mage_Paypal_Model_Cart */
        $giftwrapPrice = Mage::getStoreConfig('quickcheckout/gift_wrap/gift_wrap_price');
        $cart->addItem('Gift Wrap', 1, $giftwrapPrice, 'giftwrap');
        return $this;
    }
}
