<?php

$installer = $this;

$installer->startSetup();

$installer->run("
		ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `giftwrap_amount_refunded` DECIMAL( 10, 2 ) NOT NULL;
		ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `basegiftwrap_amount_refunded` DECIMAL( 10, 2 ) NOT NULL;		
		");

$installer->endSetup();