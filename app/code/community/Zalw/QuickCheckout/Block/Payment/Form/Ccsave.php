<?php
/**
 * @category    Zalw
 * @package     Zalw_QuickCheckout
 */

class Zalw_QuickCheckout_Block_Payment_Form_Ccsave extends Mage_Payment_Block_Form_Ccsave
{
    /*Set ccsave.phtml to change the text "Names on Card" */
    protected function _construct()
    {
        parent::_construct();
        $this->setTemplate('quickcheckout/payment/form/ccsave.phtml');
    }
}