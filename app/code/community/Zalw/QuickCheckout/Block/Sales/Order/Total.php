<?php
/*
 * @category   ZALW
 * @package    Zalw_Quickcheckout
 * @module     Quickcheckout
 * @author     ALW
 * @description: This is Block class for Quickcheckout module .
 */
class Zalw_QuickCheckout_Block_Sales_Order_Total extends Mage_Core_Block_Template
{
    /**
     * Get label cell tag properties
     *
     * @return string
     */
    public function getLabelProperties()
    {
        return $this->getParentBlock()->getLabelProperties();
    }

    /**
     * Get order store object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getOrder()
    {
        return $this->getParentBlock()->getOrder();
    }

    /**
     * Get totals source object
     *
     * @return Mage_Sales_Model_Order
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }

    /**
    * Get value cell tag properties
    *
    * @return string
    */
    public function getValueProperties()
    {
        return $this->getParentBlock()->getValueProperties();
    }

    /**
    * Set gift wrap amount in total
    */
    public function initTotals()
    {
        if ((float) $this->getOrder()->getBaseGiftwrapAmount()) {
            $source = $this->getSource();
            $value  = $source->getGiftwrapAmount();

            $this->getParentBlock()->addTotal(new Varien_Object(array(
                'code'   => 'giftwrap',
                'strong' => false,
                'label'  => Mage::helper('quickcheckout')->formatGiftwrap($value),
                'value'  => $source instanceof Mage_Sales_Model_Order_Creditmemo ? - $value : $value
            )));
        }

        return $this;
    }
}
