<?php
/*
 * @category   ZALW
 * @package    Zalw_Quickcheckout
 * @module     Quickcheckout
 * @author     ALW
 * @description: This is Block class for Quickcheckout module .
 */
class Zalw_QuickCheckout_Block_Onestep extends Mage_Core_Block_Template
{
    protected function _construct() {
        parent::_construct();
        $this->addData(array(
            'cache_lifetime' => 3600,
            'cache_tags' => array(get_class($this)),
            'cache_key' => get_class($this),
        ));

    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }
    
    public function getQuickCheckout()     
     { 
        if (!$this->hasData('quickcheckout')) {
            $this->setData('quickcheckout', Mage::registry('quickcheckout'));
        }
        return $this->getData('quickcheckout');
        
    }
}
