<?php
/*
 * @category   ZALW
 * @package    Zalw_QuickCheckout
 * @module     QuickCheckout
 * @author     ALW
 * @description: This is Block class for QuickCheckout module.
 */

class Zalw_QuickCheckout_Block_Adminhtml_Sales_Order_View_Info extends Mage_Adminhtml_Block_Sales_Order_View_Info
{
    protected $order;
    protected function _afterToHtml($html)
    {
        if($this->getChild('quickcheckout_info')){
            $html .= $this->getChild('quickcheckout_info')->toHtml();
        }
        return parent::_afterToHtml($html);
    }
}